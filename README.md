begin-syntax
============
Convenience macro for inline syntax expansion.

Requirements
------------
 - [module-declarations](http://api.call-cc.org/doc/module-declarations)

Usage
-----
A single macro is provided, `begin-syntax`, that evaluates its body at
macro expansion time and inserts the result directly into the program.

    $ csc <<EOF -R begin-syntax -o a.out -
    (print (begin-syntax (current-seconds)))
    EOF
    $ ./a.out; sleep 2; ./a.out
    1472953169
    1472953169

Full documentation is available at [chickadee][] and on the [CHICKEN
wiki][wiki].

[chickadee]: http://api.call-cc.org/doc/begin-syntax
[wiki]: http://wiki.call-cc.org/egg/begin-syntax

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
3-Clause BSD. See LICENSE for details.
